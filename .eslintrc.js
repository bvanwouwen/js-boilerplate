module.exports = {
    extends: ['@bitfactory'],
    "rules": {
        "no-console": 'off',
        "max-len": ["error", { "code": 120, "ignoreTrailingComments": true}]
    }
};
